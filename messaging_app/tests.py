from django.test import TestCase, Client, LiveServerTestCase
from selenium import webdriver
from .models import Message
from django.urls import resolve
from .views import index, confirm, save, update
import time

# Create your tests here.
class Story7UnitTest(TestCase):
    def test_can_create_message(self):
        message = Message.objects.create(name="Sample name", message="Hai hai halo")

        msg_count = Message.objects.all().count()
        self.assertEqual(msg_count, 1)

    def test_url_is_exist(self):
        response = Client().get('/comment/')
        self.assertEqual(response.status_code, 200)

    def test_using_certain_template(self):
        response = Client().get('/comment/')
        self.assertTemplateUsed(response, 'add_comment.html')

    def test_using_certain_function(self):
        found = resolve('/comment/')
        self.assertEqual(found.func, index)

    # Test for confirmation page
    def test_confirmation_page_redirected_when_post(self):
        response = Client().post('/confirm/', {'name': 'Test user', 'message': 'test test halo'})
        self.assertEqual(response.status_code, 200)

    def test_using_certain_template(self):
        response = Client().post('/confirm/', {'name': 'Test user', 'message': 'test test halo'})
        self.assertTemplateUsed(response, 'confirmation.html')

    def test_using_certain_function_on_save(self):
        found = resolve('/confirm/')
        self.assertEqual(found.func, confirm)

     # Test for saving message
    def test_saving_when_post(self):
        response = Client().post('/save/', {'name': 'Test user', 'message': 'test test halo'})
        self.assertEqual(response.status_code, 302)

    def test_using_certain_function_on_update(self):
        found = resolve('/update/')
        self.assertEqual(found.func, update)

    # Test for updating color
    def test_updating_when_post(self):
        message = Message.objects.create(id=123, name="Sample name", message="Hai hai halo")
        response = Client().post('/update/', {'id': 123, 'color': '#000000'})
        self.assertEqual(response.status_code, 302)

class Story7FunctionalTest(LiveServerTestCase):
    def initialize_selenium(self):
        opt = webdriver.ChromeOptions()
        opt.add_argument('--headless')
        opt.add_argument('--no-sandbox')
        opt.add_argument('--dns-prefetch-disable')
        opt.add_argument('disable-gpu')
        return webdriver.Chrome('./chromedriver', chrome_options=opt)

        
    def test_retrieve_objects(self):
        Message.objects.create(name="Sample name", message="Hai hai halo")
        Message.objects.create(name="Sample name 2", message="Halo halo hai")

        driver = self.initialize_selenium()
        driver.get(self.live_server_url+'/comment')
        self.assertIn('Tambahkan Komentar', driver.title)
        self.assertIn('Halo halo hai', driver.page_source)

    def test_add_and_confirm_new_message(self):
        driver = self.initialize_selenium()
        driver.get(self.live_server_url+'/comment')
        
        user_txtbox = driver.find_element_by_id("name")
        msg_txtbox = driver.find_element_by_id("message")
        user_txtbox.send_keys("Test user")
        msg_txtbox.send_keys("Mantul mantap betul")
        msg_txtbox.submit()

        time.sleep(2)

        self.assertIn('Konfirmasi Pesan', driver.title)
        result_user_txtbox = driver.find_element_by_id("name")
        result_msg_txtbox = driver.find_element_by_id("message")
        self.assertEqual(result_user_txtbox.get_attribute('value'), "Test user")
        self.assertEqual(result_msg_txtbox.get_attribute('value'), "Mantul mantap betul")
        result_msg_txtbox.submit()

        time.sleep(2)
        self.assertIn('Tambahkan Komentar', driver.title)
        self.assertIn('Test user', driver.page_source)
        self.assertIn('Mantul mantap betul', driver.page_source)

    def test_retrieve_objects(self):
        Message.objects.create(name="Sample name", message="Hai hai halo")

        driver = self.initialize_selenium()
        driver.get(self.live_server_url+'/comment')
        msg_list = driver.find_element_by_id("message-list")
        first_elem = msg_list.find_elements_by_tag_name("li")[0]
        first_elem_txtbox = msg_list.find_elements_by_id("color")[0]
        
        # Set color picker value

        setter = "document.getElementsByName('color')[0].value = '#BBBBBB';"
        driver.execute_script(setter)
        first_elem_txtbox.submit()
        time.sleep(2)

        from selenium.webdriver.support.color import Color

        first_elem_rev = driver.find_elements_by_tag_name("li")[0]
        self.assertEqual(Color.from_string(first_elem_rev.value_of_css_property("background-color")).hex, '#bbbbbb')
