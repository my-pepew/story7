test:
	coverage run --include='story7/*' manage.py test
	coverage report -m

run:
	python3 manage.py runserver

migrate:
	python3 manage.py makemigrations
	python3 manage.py migrate